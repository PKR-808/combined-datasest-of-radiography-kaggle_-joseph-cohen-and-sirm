from __future__ import division, print_function
# coding=utf-8
# Imports here
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb
import cv2

import os
import torch
from torch import nn
from torch import optim
import torch.nn.functional as F
from torchvision import datasets, transforms, models
from sklearn.metrics import confusion_matrix, classification_report
import random
import time
import pickle

# Flask utils
from flask import Flask, redirect, url_for, request, render_template
from werkzeug.utils import secure_filename
from gevent.pywsgi import WSGIServer

# Define a flask app
app = Flask(__name__)

MODEL_PATH = 'models/combo_70ep_vgg16classifier_bestmodel.pt'
# To load the VGG16 model

from collections import OrderedDict

def vgg_load_checkpoint(filepath):
    checkpoint = torch.load(filepath,map_location ='cpu')

    if checkpoint['arch'] == 'vgg16':

        model = models.vgg16(pretrained=True)

        for param in model.parameters():
            param.requires_grad = False
    else:
        print("Architecture not recognized.")

    model.class_to_idx = checkpoint['class_to_idx']

    classifier = nn.Sequential(OrderedDict([('fc1', nn.Linear(25088, 1000)),
                                            ('relu', nn.ReLU()),
                                            ('drop', nn.Dropout(p=0.5)),
                                            ('fc2', nn.Linear(1000, 2)),
                                            ('output', nn.LogSoftmax(dim=1))]))

    model.classifier = classifier

    model.load_state_dict(checkpoint['model_state_dict'])
    return model

vgg_model = vgg_load_checkpoint(MODEL_PATH)
#print('Model loaded. Start serving...')

# Image pre-processing before making prediction using the model
from PIL import Image, ImageOps

def process_image(image_path):
    ''' Scales, crops, and normalizes a PIL image for a PyTorch model,
        returns an Numpy array
    '''

    # Process a PIL image for use in a PyTorch model
    rgb_image = Image.open(image_path)
    #rgb_image = rgba_image.convert('RGB')
    pil_image = ImageOps.grayscale(rgb_image)

    # Resize
    if pil_image.size[0] > pil_image.size[1]:
        pil_image.thumbnail((5000, 256))
    else:
        pil_image.thumbnail((256, 5000))

    # Crop
    left_margin = (pil_image.width - 224) / 2
    bottom_margin = (pil_image.height - 224) / 2
    right_margin = left_margin + 224
    top_margin = bottom_margin + 224

    pil_image = pil_image.crop((left_margin, bottom_margin, right_margin, top_margin))
    # Normalize
    np_image = np.array(pil_image) / 255
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    np_image = np.atleast_3d(np_image)
    np_image = (np_image - mean) / std
    # PyTorch expects the color channel to be the first dimension but it's the third dimension in the PIL image and Numpy array
    # Color channel needs to be first; retain the order of the other two dimensions.
    np_image = np_image.transpose((2, 0, 1))
    return np_image


# Implement the code to predict the class from an image file using VGG-16 model

def vgg_predict(image_path, vgg_model, topk=2):
    ''' Predict the class (or classes) of an image using a trained deep learning model.
    '''
    image = process_image(image_path)
    # Convert image to PyTorch tensor first
    image = torch.from_numpy(image).type(torch.FloatTensor)
    # print(image.shape)
    # print(type(image))

    # Returns a new tensor with a dimension of size one inserted at the specified position.
    image = image.unsqueeze(0)

    output = vgg_model.forward(image)

    probabilities = torch.exp(output)

    # Probabilities and the indices of those probabilities corresponding to the classes
    top_probabilities, top_indices = probabilities.topk(topk)

    # Convert to lists
    top_probabilities = top_probabilities.detach().type(torch.FloatTensor).numpy().tolist()[0]
    top_indices = top_indices.detach().type(torch.FloatTensor).numpy().tolist()[0]

    # Convert topk_indices to the actual class labels using class_to_idx
    # Invert the dictionary so you get a mapping from index to class.
    idx_to_class = {value: key for key, value in vgg_model.class_to_idx.items()}
    # print(idx_to_class)

    top_classes = [idx_to_class[index] for index in top_indices]

    return top_probabilities, top_classes

@app.route('/', methods=['GET'])
def index():
    # Main page
    return render_template('index.html')


@app.route('/predict', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        # Get the file from post request
        f = request.files['file']

        # Save the file to ./uploads
        basepath = os.path.dirname(__file__)
        file_path = os.path.join(
            basepath, 'uploads', secure_filename(f.filename))
        f.save(file_path)

        # Make prediction
        probs, classes = vgg_predict(file_path, vgg_model)
        #probabs = 'Covid = {:.4f}, Normal = {:.4f}'.format(probs[0], probs[1])
        if probs[0] > 0.5 and classes[0] == 'normal':
            result = '{:} = {:.4f}, {:} = {:.4f} \n\nThe patient is in normal condition and hence, do not have Covid-19.'.format(classes[0], probs[0], classes[1], probs[1])
        elif probs[0] > 0.5 and classes[0] == 'covid':
            result = '{:} = {:.4f}, {:} = {:.4f} \nThe patient is infected with Covid-19. Please take necessary precautions.'.format(classes[0], probs[0], classes[1], probs[1])
        else:
            result = '{:} = {:.4f}, {:} = {:.4f} \nSorry! The model cannot predict the result for this case.'.format(classes[0], probs[0], classes[1], probs[1])
        return result
    return None


if __name__ == '__main__':
    # app.run(port=5002, debug=True)
    app.run(host='10.10.10.54', port=5000, debug=False)

    # Serve the app with gevent
    #http_server = WSGIServer(('', 5000), app)
    #http_server.serve_forever()
